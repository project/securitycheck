CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This is a drush plugin for running security checks against Drupal sites.
These are basic security checks, they aren't intended to replace more through
reviews of a site build. The tests are designed to stop a badly misconfigured
site going live.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/securitycheck

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/securitycheck


REQUIREMENTS
------------

 * As you can run the security checks against your Dupal site using the Drush
   command. So Drush Should be installed.


INSTALLATION
------------

 * Install the Security Check Kit module as you would normally install a
   contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * The module has no modifiable settings. There is no configuration. When
   enabled, the drush command "drush secchk" can be run to check the website.


USAGE
-----

 * To run this against a site, simply run "drush secchk" from the docroot or
   using a site alias in the terminal.


MAINTAINERS
-----------

 * Gaurav Kapoor ( https://www.drupal.org/u/gauravkapoor )
 * Anmol Goyal ( https://www.drupal.org/u/anmolgoyal74 )

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
